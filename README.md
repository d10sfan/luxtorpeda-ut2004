## UT 2004 Wrapper 
### Using Original Linux Binaries from http://treefort.icculus.org/ut2004/ut2004-lnxpatch3369-2.tar.bz2


### Thanks To
* https://github.com/dreamer/luxtorpeda/issues/45#issuecomment-539770152
* https://github.com/alanjjenkins/archlinux-package-ut2004-steam
* https://github.com/tim241/sdlcl
