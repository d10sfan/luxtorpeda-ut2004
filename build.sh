#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"


pushd "sdlcl"
make
popd

cp -rfv "sdlcl/libSDL-1.2.so.0" "13230/dist/"
cp -rfv run-ut2004.sh "13230/dist/"
cp -rfv "ut2004-lnxpatch3369-2.tar.bz2" "13230/dist"
